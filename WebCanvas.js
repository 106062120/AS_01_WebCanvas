var curcolor = "black";
var curtool = "pencil";
var eraser_col = "black";
var eraser_on = false;
var curSize = 4;
var text_out = "";
var text_size = "20px";
var text_font = "Arial";
var canvas;
var context;
var clickText = new Array();
var textX = new Array();
var textY = new Array();
var clickX = new Array();
var clickY = new Array();
var clickDrag = new Array();
var clickSize = new Array();
var shapeX = new Array();
var shapeY = new Array();
var shape_type = new Array();
var shape_color = new Array();
var shape_cur = "";
var cir_rad_cur = 0;
var rect_x = 0;
var rect_y = 0;
var tri_x = 0;
var tri_y = 0;
var shape_rectx = new Array();
var shape_recty = new Array();
var cir_rad = new Array();
var shape_trix = new Array();
var shape_triy = new Array();
var onshape = false;
var on_shape = false;
var paint;
var ontext = false;
var text_pos = false;
var cur_cursor = "pencil";
var clickColor = new Array();

window.onload = function(){
	init();
	draw();
}
function init(){
	canvas = document.getElementById("MyCanvas");
	context = document.getElementById("MyCanvas").getContext("2d");
}
function getcolor(cur_color){
	current_color = document.getElementById("selectcolor");
	if(!eraser_on){curcolor = cur_color;}
	eraser_col = cur_color;
	current_color.style.backgroundColor = cur_color;
}
function gettool(cur_tool){
	cur_cursor = cur_tool;
	ontext = false;
	onshape = false;
	text_pos = false;
	current_tool = document.getElementById("selecttool");
	if(cur_tool == 'pencil'){
		current_tool.style.backgroundImage = "url('data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAOEAAADhCAMAAAAJbSJIAAAAkFBMVEX///8AAADz8/P09PT+/v719fX29vb9/f37+/v8/Pz4+Pj6+vr39/cEBASkpKS0tLSOjo7s7OxdXV1CQkKysrIlJSUdHR07OztHR0chISHp6enf398sLCxhYWEaGhpzc3M1NTWcnJzCwsJWVlbX19dxcXGAgIBNTU0PDw8wMDDOzs5YWFiHh4eenp4TExPFxcUo0spWAAAQj0lEQVR4nN1d60LbOgxO09wTLoUyWEvH6IUyxs7e/+1OE1uO73ESOw7jB7RFjvXFkvU5Uu0gQD9RJLwIVC/cy9q9XPOTZ+htlOfkBfpPmpEXqUpWEJHJBgpZEOkj20dN9FMW6OOoLNHHaVliuSJDL7IiHyBbdsrC5QIQIbI5yA7rGq7b/BQJ+jiqEvRxmlToElmCL1HCC0oW95IUAS+bgSzWCC6Xgyx1OSwrdC2TFboW1cw42eaqxRJ9HMVL1Eu+jFHLMqxwgxBfIlnilsslViRMAla2AtkYy6Ygm7WyBStLuk6h60zS9QA1G5sF3Belu1uGvGwYKwGGfQCqZAuQFbte8l2LajbGm2PLbW9jaBVg1CpNZAWAypshAozbrtVq4q6TIoJxrBUJBYAJ1zJuAYKJjhnBpAXIdZ0JXbf3tpXlui7Ee9tcDkcNelT8magIUGmi3WqSrrmWBsb9RXwQZHEv3SZq2QclJmrgg6KaSh9kAaqVnpcPmqjJ3Vvci7cwoYmZWhM1VjOt/0SVG4CRiQ/2CBNGADkTTWvaGJWJEx8ca6ICwAE+mFdNxC8751/LPqijaib3tktN0nUe1/+J8DrkHwwTORLBEf+LULVeJgpdQ0vVrZlFmOhD1Xg14d0/QtUo2YQG6MEHwwEAh6gZMb38O1SNqNlE/DSZKkwMpGoB/gSeapj4IL5cVj/Qicpqxiv6oMyCcHv82O/3H9uwt5pV3VFe9h/7yahasNy/PX1f4J8f92/7MIVnTQZq4ogPimjsxJeJHk8A7uoKXu0+WFl1mCBds0q7AjggTHycFzzA5u/jXkvVhId/3QD9+OB2dwEkAEQvzsfMXE38znGYoGQNw8T1Qg3w8nMLsp1TBfvOyLin8MFyR7meALD+ex/L1eRNNEK9zI2qrR71AOsXD1sNVSNq5vU/8mR6qqY30fCbBCA166BP/jsWnSbapHDSAhIfc1nRswDrX98Pp9PhWXDGTZeaRRPxMz6C+l7Rr74zA/fr9qMWr3U6/nlkAsdDFmjDRBXXAhDxZxMmVg80wKe/EdP13wNtvDszNccDtLmiD3/QANdc10UV/aW9cx30Buh7NbGiAT5ueIC1bHWgRLZJp5qSXuwB7B8maBM98H4Far4sWpYaYDWVeVolQD8+SE8yd6ECYJDvWmfcdqiJ3XgmVI0JE7+XKoDJMronzrjTq9lkR0maf1Y+uCtVAOuuV+1ss9GZaFOrQSK+7+QLGyY+VQAJMceyfwI1wKopOYFSG+9U7QdL1W5VANHlnmG2eSrUxSBxk3lKnQDsTdVWAhe91QFsVlc4YHSpCe9mRNXwi89gqUy+xEuQXdwYAfQeJh5kq4nPSHYzkJrBDmQ/XQF0R9Vaan3LA6SWS2s6XvQA6D9MHN4B4BWaURUPHrYw2s+5bhzYujZfK3raRO/SYwuwnm5kfKS+XEkalZkaYInq2vo/+HVG1XYXJnNsAV4gliLARs3yCsw5XCoBsnVtnpIvDFW7ay53pL1S9VSt+AYSG4jvwjjEbF2bJx9kAOL/HMmkg31R7Dpd/QdCK6WazdJKUtc2AuA4qraDy5Uf1GxTj6I42ccrMvEWHWr2Bmgz+cKEiR21XNpSs83iNl0KnlRtYJC/l5YBujNReAJYd31LzzZAwxlPuoF7cDAD6H9FDz4oAgTewj5VewORNyOA/qnajl7w3i7apAWh4Zya5B5c69Tk6to8rujvRIBPxxZgDTFm1Pwgg7zVqAl1bf6p2k4w0cVTjkM/+CJ+co1X9GcA+JgKtK41Z1zXNgMfpIsQMMDmITeVYcOhHwP8S62x1GqydW0eky90mMAA79GIfdDOeNuqmT6S1h+JWk26rs1n8uWO7hoDRNdLyiM121wgQtnXG2l9n3aqaQHguBW9GCZqH4Suj++UM36W6HLrtvU+mhbgkDAhGcG8lQ0+xSdwN6173ge2ADpb0d9JfBCbaHO5a0oW+yIFcPGhVBN33bwVaxqno2o7gaqhWRS6ZgCi0N8CvGr4jFbNHNW1qR84Tk/VWoDtg9+f39rZZnGiAP6kb4ZMTVzXxlfETbui5wAyPogBxkzoJ3QVHpVq1ERZbj5B4JWqSXzwIaZD/xUFcN+lJlfXNjlVk5loSnWNAD426n4sRIA3nWriDoYDHLmil0wyook+1nNtHuZHAeDaVM3xAG1TNdpEnxHAmHsCZzSCowGOpGr0BK4E+GsZkHC9ZSaZSUfQNlUTTBRdbr8YMoIR+ngWVC1tZSFMEBMNAibQm48gW9fmlqoNCxMmADVqsnVtE1I1yYK3XU20gV4F0NhEi6rJcmf9AdqnasowkYgAjU20imsRyHI79cGwP1X7pfRB4xGsoOBfDtBq8oWZRdUretoHqTAx0AcThtJMn3zpoGoIoMREjUdwOEBLyRetiT6ODxOVHuAkyRe1iT4rTbSHD2JIEfrYIVXrSL7AgldF1QaOIFGTr2uzQ9UGhAklVRs3glET61PYgMhH8qUXVesPsNm9hdS19VvRz5mqtePA1LV5Sb6IYcICVRPUNAZoP/nihqrxag4AaGqiXVRNMota9EE5QE0c7L+i70/Vng190LWJGvqgefLFAVUjakJdm+swMZKqDTfRlK9r85l8UYcJFmAvRonq2kpgJ5PVyciSLy7CBFvX5mFF74qqtYySrmublKpJki8ufFBeFeUv+WJpRS8+eOi6NZMkX5LAXpjg1exqOW5FL5tkpqFqZKpofltd0fdPvlilaiEHENW1Ja6omhjoHa/oBTVzZveWCZIvZydhQqBq7TgwdW1+ki9OqJq8rs0CVVsyYcKMqiVmAM0YpWBoqK4tUNyakVRtquSLgZqKln6TL2OoGr+ZtDnAjjAxF6qmGEHLyRePVI0H2LzNbfugJEwo6mRcUTVlXZur5ItzHxRMFPsgX9c2ZZ2MveSLZodCvq5tSqo2SZgomN1b3CVf3FC1PoxS2fJLJF90anYA/GpUzRDgFDn6n4YALT14UN0aH8kXm2GCAIwULQclXzRhwnKdjME4YIAZqmv74skXjYmydW39fdB0Re82+aJRs5LWtVlLvqh90FWYEDZCkda1uQwTahMdkXzp3rEe3o0yUX/Jl+6NbPlbMyRM/J4jVesE+EWoWk+AxsmXJW2iv42KECxQtUH7nac0bmvJl3NEKT1B8kU9gnAqmc/ki12qxo8g3r0lg1tjmaqZ5CZsrujFEcR1bSm0tEPV/K3oBYDsqWTGyZeuFb3Tkmbjcp4GIHto11dNvhgDNPTBUrn9ps0wofFB40DfdeyaquWL0YLX44q+00Q7NvPfGC2Xpk++qAGiiF8ZH6hB7WXwMPLbZ06pGnsqWdCeStbJgXag4uXviwBQ4oP9l0sDki/qs3dS5lQyA+PG22stkK2+BJ1hYpLki2YXZJTlxkcfd5sotWcostUX5Aizo2rcOQLKujaxZfmbAXiBmBXeky+aEYTjoVBLA+NOVhzAxeI0R6rWsh4aoMmtKdcswPqr42+vdJiYwYpeda6cgYkul8FZAIiVvo8o2XlQNWKikSnAeIlnUhlAJyv6PskX2Qji+QXVtZkdz0e2tX3f/6ABHpQrem8+SExUUdemmH8PMILnYEt2R72CXXA8Jl/UI5gxp5J1AVwRE123G4xeQeifE1VrTxHk6trUPtjYyZr4YJiH+ZZ2xhfbT7bthAl2a4zulncA8BzUdrJ9p2abU8P8ZkLVhF2v+JYKDhQSXa+LpiWz1chLMHXyRUnVhIMuuwBCtF2TZQVsTMxs9vvS00SdUbXOEZSFieaHnGN3wDS8KI7vVIR8WnhMvsh8EAOMEO7OhVZEOGm9ISr0sqV3/JsVVSMA+VPJlMZdXhNdN1QvWynAGYQJAFgydW26U1xbTnpgetmC7TZIczOAvaiaQfJFc+waU9em9kFqa+krVFDQljoc31uA9Yw6qzDBn0qmuzVrEt83bC/BtgVYQ1x6XNHzANmioY4AcwcAn1iAOXUKQwMxtQJwzIreHCBtJysAWBspf6BGS8PhCZyfMBHpAeqNe00YWtjKgtIB+CI8gfOQfBlpooiTNkqfpY83ttRss3jzuaIXAUbcbeSoGl5oxcTTriPORFEvW2q2WQxaTVimaqQygd29ReaDzd/2Yf4q5kwUK31hN3Ton9wHOYDKujbVUvlMppLXAABmrNLb1hf9UzW+ri1XtQTjJg/zrxaYlUpo3fGbFYBWw0QiP5VM/CLtmh6e64DzweZFGWxe9ABt18kYmCg5wUxxa0jL/Mz41x8aIH5kd3x9Hj6CtqmaEM2ULXEEjZlYUPtiyo7g9vXJkg9apWp8ZaJo3EAR0jUBuH1EIQNkG3j34KVjAdqlaioT5e2kbnkH2j8F8c8F+GJUZcHmzzMd/nyECRVVI56E6tq0p5KtiPY1J/0FoxhvXx/Z8NdGfP9hgsxvuK5NO/Zrov1lBV9usKFSvicAfOncSNw1VePr2nLuNrJj/xu0PzTrwbj1OynA003I3UbXyReNiSbsqWShNMDERPt1UH9RcXNLz6sswPfTvsRJcw9hQnKKoOpUMubWrMnwrKIEz5wyCrr4cYF3sRyuF1d1MlqAHKPkWvLGfQYYOxIYBIC17+3DQBJtfVA1nnDhXmRhov5ZkWBAjqoTAe72GaP0lMkXQ4DqsV9Tiz0pwP9Oe1zn7XYjW72a6iOfcC8ik4GH/gcC54oHiKaWOKg4s/OQfNGMdrO6qJQA4fmE1PUuvrekNBp55sAYH6yUJgqnkik5EHmYLwI87RW9eFzRC+4vnEomRND0Tgrw2+4m5HpxeuZAMNAHc/ZUMplxFzKAp78rOCdrLEDLYUK4GWj3FryIlXKgmHe9i3Fe/h13rjodJl86qZq8Kkox9hld1l3PnHJZj2FCfW9JVlR/a14JwB+nm4Rtad1Ezetk1CaaKgCqjXsHgWGTk5vhCKBVqkbUjJheJA8cy/3u8WWfBmVsbCc+V/QCwCbip0nn/BsIYz8SoN3kixgm4HIZU9dmNP9aNlH1l5THhQnomqlr6zP2RmFiquSLRk35qWQaDuTVB4eoSde1GSyVrYUJy1StU01FS/c+6IqqGY7gSB+0k3yRPxvrqaaipZoDzZKqaUw0Qr30mH+dUbURyRcZQHw5vq5thIl6TL7oyr6YLLcrgBNQNVU0Y08lcxUmRKrmakUvqol2b8m5lhNSNcdhgqjJt7QaJkZ9SXnQil5Uk205J6pmlHzpVlPeywRUzWryRaNm717mQdXUJsr7YIQ+noyq9fiSsh0flJ9KNk+q1itMgCx7KtkXo2oyNXkTrYpmCEld25ypmsmKXlSTq2ubFVXjuh4Yzagc6ddZ0Zv7YHddmx2A3qgaD9AxVRv1zZdRjFJoOUOqZpB80cg2V5XUtX2t5ItETdx1xJ9K9q9QNaImW9c2jzBh8wG84lSyr5Z80ampqmsbAXDCMKGkaoqioTlQtUHJl241UUvYGiOP4TFcDN8shW/JoMc6tfaJIAtMPs7gcthEQTYTZCuJbIBlC0EWd52IXYtqcl2jlkWB3qUVLKvgRVnhlUdZpN2yOYjgXkA2b2UzQZa7XC52XRl3HRS8muhdBntFlfiaaQlZe3iR4f2yopKXzVtZLJLhjKRGllwOuo4Muh6kZtr+pl5EaSS84ET6yMpEoh6X6yMrqBn9D9e0LHFOE3ilAAAAAElFTkSuQmCC')";
		curcolor = eraser_col;
		eraser_on = false;
	}else if(cur_tool == 'eraser'){
		current_tool.style.backgroundImage = "url('data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAOEAAADhCAMAAAAJbSJIAAAAh1BMVEX///8AAADz8/P09PT+/v4EBAT9/f38/Pz19fX39/f7+/v29vb5+fn6+vr4+PiNjY21tbVzc3PMzMzT09OJiYmQkJBoaGixsbHW1tZsbGzGxsbb29tlZWWenp56eno4ODhISEgYGBicnJwNDQ3q6uq8vLynp6cvLy9bW1sWFhZTU1MgICBAQEArLCN8AAAQ7ElEQVR4nN1daWPbOA6lYkW3nJG9440TT7PTpE073f//+9YUD4kEIJESdXT9obFdHngmQYAPIMWYeMUxeMOoN0uVZSx/+Xr7fI+in2/X7+eCda9ZXbevtBAf4zTVb8T/JIV+k1BlQRGsLCPKqiKseLlF/df7l2c22vW4mOJVH8XXcVWJr5OqluWO8qcsjumEstVoWdUc+/YWgdeP1+ldq3bb17EUX8d5Kb5Oylw0UZSVKFKVNSgreymPzC5bqLJSItVcqsrWXXOi7Mc/CtXjY//NF6Ms6BqKWVhlWwmOB/F1nB2EROkhEzXrh1xWeJBNlAdZ83CQQj+UsnFVNn+QEmWybKLKFl3Zo1n2W2QDfJRvPl8r3fUEMds5W0vcd6HHaz7YZR8yZpbtAD44A/xKAry/TlTXB7trKGY7eVM5c/VPkzwEBRh3ADMC4BMNkP89G11nXde0mLLr8hirceSCPACApVUz6wCqKaqFnjCCpSz7ryGAj2IUu9+2E9P6bXtiqt+2RSKtRn9U/EdwEsBuBB+HAd4hJkjXY2Lqrq2aDpM7rA46AIyilxliik/Z+BQNrIOliw6qN1H0TIlJ6qAJkBZ6cx1Ub06uYlq/rfjkYmA200H15sSmWLOE/4nzZexg7ALQHsHeFzbSU+EgpjVFkzrhTl65iA5OmqJR9Ov7a1Kfn6IIrDbS9PvoYJq3Fr8aXX8D62CuAdpTNIq+iiLs4wcYSgHRQUzddZrx/4nlPmQPZiKK/lBdF6wBADlEPzHbItLi78FV6wHkZa8A4N2BKzymqOpaAaR+mtXMRA+g6LqJbIDa9DuYCRPgHlw1awR5M1e7iNRFNzHLPsB96aAa7UPSWAClG+4jZiwk2t5VQwDyrhsAkG+mPHSwtfhJuZaZKAfMhKWD6rdtAMC7LiZATIp4KGq+QazylXf0TlNUds2ULnarjjL9LmLmXKi09l9/g7tqFMC4LBsLoNr1O4hZZbzHWLFvQzpoTdHgrhoxRfmOninT3wPIITqYCd21KbQDwOCuGjmCYlQaZOfxUpGuGiD/xgFupYO66wZxw1+cxZSfFjYTk3RQ7ejj8mIB7DFwo0uF+clpcq9jJjpWjU872w2Xpp82E1rMWEi0I1cN6zq23XDJwKWkq6bFTPl/pOWedVB5lA1cbSADB8SsuSlMjtWyAL1dNYbx0z03vDP9Z1xMPYLH1uIXKlqzwY6e1kHw2x7ixgIodXHATOQZb05Z/F1PUdF1AwAaDBw9DvMBBjcTBHXfwE1/x8DRYpoAt9vRuwRfGtiKdMOnjODGrhpGPFT11WqlY+DIOC0JcGc6KMrWphveMXC0mLGQaE86OBJ8ucJNP2fgKDFbLdVh/l3o4GjwpUEmxAslZpurkRzrsV42cNXo2ERKMnBQzLxNOVGpNjvTQTrLAmXgzqiYWRt5ShYB6DRFR101glkZZuCAmOrTHl01ivxDGbh4EODOpigF0IGBCwhwKVfNJT5IM3DjAHfoqmHEA87AnZBxMPPa9mAmnGJEvMgVAOQMnA2wFnlt/sTvhjo4yMCdLIBmXttGwRc2MU8mKa8WQM3A6XHIzLy2DVk19zyZ3o4+H2PgkkNJ5LVZvexyigoxG5yBs8T0Bhg6+OKUr0Ro0gADNx3ggmaCDmNS/HRMMnAA4A539AN5MrrrAxtj4EAvzOxlxzqoylIMnKRZzby2/e3omUPwBWXgZB6Uymvb3FXzSaeEpFPP9OvOJQMn89pC6uCCrhoVfDmSDFxh5rXtVAcdHK6CYOAKI69tg+DLLB20JloDAN4h1kZW1E539MBVo4hfjIE7YQB3NkXdd3W1zcDxvx8+ANcKvkxN58lihIH7ZJLzTrYzE247erfFvs/ASQv5RUS5k5I+fbZLV40SE2HgnuM2r02dCfw9dJAWkwE3PPrRSl2P1dyjq4aJmeWW6b//ee2i3Lvc0Q+4apiYwA1/5GdQ2QyAG7pqlJh2Dty7pJDn6qDHFHU62jOD/LMZuPMMgNOCLx7M9hT6NsmtoyjfpwNcOPjiYyb6Yh5Lczm9iq9/CzPhulQ8GfbijfXy2n47Vw0X82yw4H/38tp+S1cNEbMyiLfHLq9tT2Ziog6Ksqmxv3jUUe5966DPWvhs0DY/GSNq7iH4MmlXx/7SQnGpPimAv4+rZolZ57/6AKObP8DF0kjCpPOkfxoA5aHUxYMvS7tqnZjFv02AIk5T7SVPhs0zE3wEbYARr1yU/lN0Bzt6dAT/sAFypy1RFxDt0kz4AQQj2E5Slde2hzyZeakEqT2Cj61Xqiz+voIvQXTw/lfGgx0B7nBHP6iD97//rAhwZvBlgg6apLeDHdyTq4ZNUWQEo28A4IbBl3muGjpFo79Yl9f2f2cm+N8/mYxy9/PaltnRh3XVEE1CR/CJKVuvbm/5TXf0hJkQAI28tkk6+Kjuk9mVq6anqJHXNvFOp+j9PAJw8Ss3aB3Es6L8r/573XRHj5uJf7Vd9wFO2tGrNp/jLc0EOYLqtx0DODRF1TcqI2AnrpqcorLr2PgZfXb04EK8Xezo9SqquhZ5beUEM2Emrr4y3EwsEnwZcdWe+l2nxu0tXmbCenNeM/gyPkUVwMLIa5t39d+p2pGrpssaeW1+rhpYbaQurhR8YYNmQumgymtj1k/j4qr9B2n8me3EVdNT1Ljty3c3cUHm6nm94IuLmdBR0UkAGYcIlPFc7MFVI0bQP/hyQ0y/zArY1FWzAcailwnBl+QW2b3I5WYPrpoua+e1eezoDxKimUV+9g2+hAUIdFDe13YkAVJTVOwmLgAgH8UduGpdWXF7S20BdA++XABAvtxYQgd31dzNxNG4vWVS8OWGnAM4EwCX39EDgKprHKALq3Ysb1YR/uaMAlzPVXMGODZFudAJuwGAfKexhavmCNCfVbsAgPdRrNZ01XAzEWCKqml3oc8BrL+jhxeztd/XcwCy8maVVW74Oq4abiZ01y1Xqh/+Mi34kiUXC6Bm4FYIvhBmQnVdifvaahOgf/DlCgD2GTiPky+BXDUtZp7zn9TOa5sSfMHc8CEGLijArixYwDPenJ3XNi34QrrhwYIvE3RQLxXy05zgS3a82P1LBm79HT0BcG6eDHDDLQZuA1cNBzgnAHoBAPkRwLV39KMAfXTQJn5vNAO3dPClB9AWU0S5J5kJO08mzbG7cJ/Z7B39FFdNiymfShYmT+YIGDihi+vt6KGY8vaWQgL0mKIEs30BAPsM3GqumhazfSoZk1HuMHkyl8gGqBm49c2E+VSyQHkyqe2G65s41nPVgJjtK1SezMFywzsGbrUdPQ4QPn9w+smXCwDIV9RJrho6Ra3gixvAb0MAvfNkrnDTH52mnOOcp4N9gB8+I+gQfLnB5uRys/iOHogpLD58yOmsXLU8u1jNAQZucTOhjzm2eW3fPAA65cmMMnCBgy8D5zhFlPuNBjg5Twa9C7da20yIp5Kxl0lTdCz4cqMZuMVdNdtLtK+vCXLyhWUkA7eaDqqlonIH6JMnUzKCgVvNTGhr9hJUB/s7eoyBOycLb3ihmOK0XgdwPF/UPUZ/AQA7Bm5pV02LGRtO1mP0iwI46eQLxsC9GgAxM0G7ah4A9WMxEvZmLAbfqZpT8mSy6mrLKpebpXWwIx7qmP2M+vTKKz6Ck9MpcQYulJkYHcGCR7m1BrZtpsMA/fNkcAZuHTPB6vapZO+GrlTBU5oHGTj37dIUMyFjaua54NO8ky+Q+C1QBu7sMYLerpot5n+NNp9Y6EPKlXLDLQZubvDFXZPMu3ijj8KqGSCdEmPgnouFdBBMNPbdaFNcHeXnqo0HX7pdP2DglnLVtJgxe+71yxtvqJ+GBjgafKEZuDDBlyHyL2Hp35Zv1YQ/+XJICQYutC8KxGyfSvbFXs6b+BDCTJg7+hsAyB24MMEXumuR1/YaWY1HV0nihLkhVm6XBhm4WcGXgd82U9txu/HGrunnqmEABxm4QDt6OA6y61fQOIcY+pAyzsA9kwCnu2pAzDZ2bzuPTUuZ8Vewky+xCr4ZQ/kazkxQYvLXJ9yqXlUWcZAbYgcYuBNbRgfNvDa12PRNfyNqhj35coFu+PufFMApO3oopshrq05ILw0LfvIFZ+Bg177BF6zrfl4bfyrZGa42d4hx6JMvpeAu4ZYxsKuG5bWdEO+4KcsZrhpBGxq8EApwvqtm57WJ/k8RzNm+UjXnpFMiDNxSZqI0nkqWAHZfmf7AKc0IAzfHVRuYopqAl5+ELlrL+TX8yZdDgTFwxhSd56rZYvZqngDAuy4mh/AnXzAGLpyrRmQmtp86XeyQXmNQc37G7wVdbcLqIBxB/lK62LdXjStAnxA2NP2BXTUtpshr008lK042QGn6A598KVLsFGpAM6GXCpnX1qt5AgA5RIdnsa6fJ+PiUZpPJROs2glRkabWye5hTr4EDb4MTNHSfCqZ3C6dO13UbviVEHqLXDWfKzfwp5IVL0j/NgM3QweDBV9ogNZSYdW8T+6T1b80/RuefJliJsysKItVOwGA91FMD0GOFYQOvjgCtIU+R3Crqhm41U++zIoRiZrgVq5eks0AAxcU4Kzgy8Bot7uL3AZYPyQnKIjJwO1JB3NyiqqnkmE+0BkAbBm47c2EVyqB/VQyw4Iapl/pombgljITQXUwNZ9KBsYeuOEdA7f6yZeJMaK2iLqvDfpA1YsNsGPgxgB6uWp+wRd/8s/8ZNQ8A4Ac4rTHQy0VfBknHgZ/mhMAyBk4FbrawlUbixHBXZ34RE7uk9LFAQZul66aHu2Y9XuBNdNxBm7yFJ3gqvnrYGvxk3Jg/UUZuGbvrlqXzmPc10ZMbpSBiw9hdJA+pDzPTMiu6zbK3V5LR9RshUYZOEYARHYTKMCAwZcBbqzNa1O3t9Bj3y03gIHbp6sGbr0ia6r1N6UYuH24aqPkH1HT6AVn4I67MBM0+Tc8gtbkxhm4wgQY3FUbT0x24KeJmkBoBwZuPTMBXLWBKRqLXsbHHmHg5HKz/MmX0SmKAZTNtfe16aeSDY89xsA1O3XV9AjWIspdudVEGbiavrF+6TwZh0fTHI3bW8bXX5SBiymA6+/oIfEgbm9JnXuhGbgtgy+0mdDm2q5JE78xzcB5BF+8RtB/NwH3BGbNkeALzsBlGwZfxsk/vBdycqMMnGxzw+DLAPnn3QvGwF1dAK5kJmwdjMXXHkkI1ckGKN1wJ1fN45ByGB3UeW3tyzFPhmLgtt/RQ4BtRpt+KplzngzKwOVyMGqaNgzrqmFi2lM0P/by2nxi9BgD90MNC5YvumTwZSiEYuS1+dREGbjo6Vylz3/9ogAuv6OH1FEvRuqZJ8NOEKDx2tJVA3m7I70QNfsM3EhC7Eaumg3QO0aPMHBh0ikHFxn/jDNQ0zkAijFwGMD5rppD8IUW085r80mnRBm4MR1cJviCiKkSR+ynkvnF6DEGbh+umhbTzGvzT0LAGLj1gy8DEw1/KplPjL6ni+QUXd9V651+4GVBXptPhLeGR1F6AL+yJc0E6aoRSUNTE4E+PnGAkXiC5PLBl3ExRc0yU/kZiobL1M3ymT5ep/JQS6PslyiCJzWi6J8P3osqW6h2q6xSzRWqa6kmmYr5qa4r0HVZOoipm+sDPB4lZZbL/otcVqhzufOojglR9vlHBF5v7SNAE1U27dpVzeUp0VwKu86prntiKkLNFlN8KgpZU0UiEnlzBUvVm6KWNWu7bBqz1y/vBr7LC1UWNKe6jh26niRm0v3bexMnMXhjFbHKFufv17efj9H75+3rS+nQXDzY3PSyQMz4f64aFPOGOoQxAAAAAElFTkSuQmCC')";
		curcolor = "white";
		eraser_on = true;
	}
	change_cursor();
}
function get_s(size_s){
	current_size = document.getElementById("selectsize");
	if(size_s == 'large_s'){
		curSize = 10;
		current_size.style = "position: relative; border-radius: 5px; background-color: white; background-repeat: no-repeat; background-position: center center; background-size: 45px 45px; background-image: url('data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAkGBw4HBhUSBxATFhARDxYXGBUYFxUWFhIWFRoaGBUVFhMaICggGB0lGxUXITEiJSktLy4uFyAzODMtNyotLi0BCgoKBQUFDgUFDisZExkrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrK//AABEIAOEA4AMBIgACEQEDEQH/xAAcAAEAAgIDAQAAAAAAAAAAAAAAAQYFCAMEBwL/xAA4EAEAAQIEAwUFBgUFAAAAAAAAAQIDBAUGESFBUQcSEzFxIjJhgZEUI2JyocFCgpKxwhUkQ1Jz/8QAFAEBAAAAAAAAAAAAAAAAAAAAAP/EABQRAQAAAAAAAAAAAAAAAAAAAAD/2gAMAwEAAhEDEQA/APcQAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAdPEZthcNXtiMRZpq6VXKKZ+kyDuDgw2Ns4qP9rdor/LVTV/aXOAAAAAAAAAAAAAAAAAAAAAAAoHaB2oYXStU2cFEX8bEe5E7UWt/Lxa458+7HHrtvEo7XdcTpXKYtZdVH2zExPcnz8KiOFV3bryp358eO0w1uuVTcrmq7MzVVMzMzMzNUzxmZmfOZnmCwai1vmmo65/1LFV+HV/xW5m3aiOncpn2v5plXPDp6R9H0Ai3HhVxVa4VRO8THCYmOcTHGF40x2p5tkNyIvXZxNmNt7d6Zqq2/De96mfXvR8FIAbYaN1lgtX4Ka8tqmLlG3iWauFy3M9Y50zyqjhPrvCxNPMkzjEZDmdGIyuuaLtueHSqJ86K4/ipnnH7tp9G6ks6qyGjE4Th3o2ro526496ifSfrExPMGcAAAAAAAAAAAAAAAAAABjtR4r7Dp7EXYnbwsLdr36dyiqr9gava+zyrUersRfmd6PEmi30i1bmaaNvXaavWqVe2RRG1Eb9H0CE7ACE7I2SCHpvYTn1WX6kqwtyr7vE0bxHS5RHL1o3/AKIeZs1oq/OF1dha6eWJoj+v2J/SoG3A47FXfsxPWIcgAAAAAAAAAAAAAAAADp5zhPt+UXrMxE+LYuUbT5T36Zp2n6u4A0spiYp2q33jhMdJjzifjundcO1fT1WntZ3Ypp2s4iqb1ueW1c710/Du1zVG3SaeqnghIAbnohIG7OaGw84rV+Gpp5Xu98qImv8AxYN6h2H5BVisyqxVyPZj7uj4+U3Ko+lMb/mB77ho7uHpj8MOVERtHBIAAAAAAAACISAAAAAAAAAq3aHo+3rHIptVbU37czXZuT/BXtttP4ao4T8p84hrBmmXX8px9VjMbc0Xbc7VUz+kxPOJ5TzbkK3rLRWC1bhe7mNG1ymPYu08LlHpVzj4TvANUR6BqLsjzPKrkzgIpv2+W0xRXt8aap2n5T8lRxOnsfhqtr+DxET/AOdc/rTEwDGjLYXS+Y4ufuMHe4/9qe5H1r2hddNdkeJxl2Jziru07+5b4zP5rkxtT8on1BTdLadv6kzGLeGiYoiY79zbhRHSOtU8o+fk2d0nkVvI8sot2Ke7FNMREdIjrPOecy+dOaYw+R4WmjDUU0xT5RHlHWfjPxlngAAAAAAAAAAAAAAAAAAAAAARMb+bhrwdq579EfRzgOvRgbVHu0U/Rz00xTHswkAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABAJAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAB/9k=')";
	}else if(size_s == 'normal_s'){
		current_size.style = "position: relative; border-radius: 5px; background-color: white; background-repeat: no-repeat; background-position: center center; background-size: 20px 20px; background-image: url('data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAkGBw4HBhUSBxATFhARDxYXGBUYFxUWFhIWFRoaGBUVFhMaICggGB0lGxUXITEiJSktLy4uFyAzODMtNyotLi0BCgoKBQUFDgUFDisZExkrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrK//AABEIAOEA4AMBIgACEQEDEQH/xAAcAAEAAgIDAQAAAAAAAAAAAAAAAQYFCAMEBwL/xAA4EAEAAQIEAwUFBgUFAAAAAAAAAQIDBAUGESFBUQcSEzFxIjJhgZEUI2JyocFCgpKxwhUkQ1Jz/8QAFAEBAAAAAAAAAAAAAAAAAAAAAP/EABQRAQAAAAAAAAAAAAAAAAAAAAD/2gAMAwEAAhEDEQA/APcQAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAdPEZthcNXtiMRZpq6VXKKZ+kyDuDgw2Ns4qP9rdor/LVTV/aXOAAAAAAAAAAAAAAAAAAAAAAAoHaB2oYXStU2cFEX8bEe5E7UWt/Lxa458+7HHrtvEo7XdcTpXKYtZdVH2zExPcnz8KiOFV3bryp358eO0w1uuVTcrmq7MzVVMzMzMzNUzxmZmfOZnmCwai1vmmo65/1LFV+HV/xW5m3aiOncpn2v5plXPDp6R9H0Ai3HhVxVa4VRO8THCYmOcTHGF40x2p5tkNyIvXZxNmNt7d6Zqq2/De96mfXvR8FIAbYaN1lgtX4Ka8tqmLlG3iWauFy3M9Y50zyqjhPrvCxNPMkzjEZDmdGIyuuaLtueHSqJ86K4/ipnnH7tp9G6ks6qyGjE4Th3o2ro526496ifSfrExPMGcAAAAAAAAAAAAAAAAAABjtR4r7Dp7EXYnbwsLdr36dyiqr9gava+zyrUersRfmd6PEmi30i1bmaaNvXaavWqVe2RRG1Eb9H0CE7ACE7I2SCHpvYTn1WX6kqwtyr7vE0bxHS5RHL1o3/AKIeZs1oq/OF1dha6eWJoj+v2J/SoG3A47FXfsxPWIcgAAAAAAAAAAAAAAAADp5zhPt+UXrMxE+LYuUbT5T36Zp2n6u4A0spiYp2q33jhMdJjzifjundcO1fT1WntZ3Ypp2s4iqb1ueW1c710/Du1zVG3SaeqnghIAbnohIG7OaGw84rV+Gpp5Xu98qImv8AxYN6h2H5BVisyqxVyPZj7uj4+U3Ko+lMb/mB77ho7uHpj8MOVERtHBIAAAAAAAACISAAAAAAAAAq3aHo+3rHIptVbU37czXZuT/BXtttP4ao4T8p84hrBmmXX8px9VjMbc0Xbc7VUz+kxPOJ5TzbkK3rLRWC1bhe7mNG1ymPYu08LlHpVzj4TvANUR6BqLsjzPKrkzgIpv2+W0xRXt8aap2n5T8lRxOnsfhqtr+DxET/AOdc/rTEwDGjLYXS+Y4ufuMHe4/9qe5H1r2hddNdkeJxl2Jziru07+5b4zP5rkxtT8on1BTdLadv6kzGLeGiYoiY79zbhRHSOtU8o+fk2d0nkVvI8sot2Ke7FNMREdIjrPOecy+dOaYw+R4WmjDUU0xT5RHlHWfjPxlngAAAAAAAAAAAAAAAAAAAAAARMb+bhrwdq579EfRzgOvRgbVHu0U/Rz00xTHswkAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABAJAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAB/9k=')";
		curSize = 4;
	}else if(size_s == 'small_s'){
		current_size.style = "position: relative; border-radius: 5px; background-color: white; background-repeat: no-repeat; background-position: center center; background-size: 7px 7px; background-image: url('data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAkGBw4HBhUSBxATFhARDxYXGBUYFxUWFhIWFRoaGBUVFhMaICggGB0lGxUXITEiJSktLy4uFyAzODMtNyotLi0BCgoKBQUFDgUFDisZExkrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrK//AABEIAOEA4AMBIgACEQEDEQH/xAAcAAEAAgIDAQAAAAAAAAAAAAAAAQYFCAMEBwL/xAA4EAEAAQIEAwUFBgUFAAAAAAAAAQIDBAUGESFBUQcSEzFxIjJhgZEUI2JyocFCgpKxwhUkQ1Jz/8QAFAEBAAAAAAAAAAAAAAAAAAAAAP/EABQRAQAAAAAAAAAAAAAAAAAAAAD/2gAMAwEAAhEDEQA/APcQAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAdPEZthcNXtiMRZpq6VXKKZ+kyDuDgw2Ns4qP9rdor/LVTV/aXOAAAAAAAAAAAAAAAAAAAAAAAoHaB2oYXStU2cFEX8bEe5E7UWt/Lxa458+7HHrtvEo7XdcTpXKYtZdVH2zExPcnz8KiOFV3bryp358eO0w1uuVTcrmq7MzVVMzMzMzNUzxmZmfOZnmCwai1vmmo65/1LFV+HV/xW5m3aiOncpn2v5plXPDp6R9H0Ai3HhVxVa4VRO8THCYmOcTHGF40x2p5tkNyIvXZxNmNt7d6Zqq2/De96mfXvR8FIAbYaN1lgtX4Ka8tqmLlG3iWauFy3M9Y50zyqjhPrvCxNPMkzjEZDmdGIyuuaLtueHSqJ86K4/ipnnH7tp9G6ks6qyGjE4Th3o2ro526496ifSfrExPMGcAAAAAAAAAAAAAAAAAABjtR4r7Dp7EXYnbwsLdr36dyiqr9gava+zyrUersRfmd6PEmi30i1bmaaNvXaavWqVe2RRG1Eb9H0CE7ACE7I2SCHpvYTn1WX6kqwtyr7vE0bxHS5RHL1o3/AKIeZs1oq/OF1dha6eWJoj+v2J/SoG3A47FXfsxPWIcgAAAAAAAAAAAAAAAADp5zhPt+UXrMxE+LYuUbT5T36Zp2n6u4A0spiYp2q33jhMdJjzifjundcO1fT1WntZ3Ypp2s4iqb1ueW1c710/Du1zVG3SaeqnghIAbnohIG7OaGw84rV+Gpp5Xu98qImv8AxYN6h2H5BVisyqxVyPZj7uj4+U3Ko+lMb/mB77ho7uHpj8MOVERtHBIAAAAAAAACISAAAAAAAAAq3aHo+3rHIptVbU37czXZuT/BXtttP4ao4T8p84hrBmmXX8px9VjMbc0Xbc7VUz+kxPOJ5TzbkK3rLRWC1bhe7mNG1ymPYu08LlHpVzj4TvANUR6BqLsjzPKrkzgIpv2+W0xRXt8aap2n5T8lRxOnsfhqtr+DxET/AOdc/rTEwDGjLYXS+Y4ufuMHe4/9qe5H1r2hddNdkeJxl2Jziru07+5b4zP5rkxtT8on1BTdLadv6kzGLeGiYoiY79zbhRHSOtU8o+fk2d0nkVvI8sot2Ke7FNMREdIjrPOecy+dOaYw+R4WmjDUU0xT5RHlHWfjPxlngAAAAAAAAAAAAAAAAAAAAAARMb+bhrwdq579EfRzgOvRgbVHu0U/Rz00xTHswkAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABAJAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAB/9k=')";
		curSize = 1;
	}
}
//cursor
function change_cursor(){
	console.log('change'+' '+cur_cursor);
	if(cur_cursor == 'text'){
		canvas.style.cursor = 'text';
	}else if(cur_cursor == 'pencil'){
		canvas.style.cursor = 'crosshair';
	}else if(cur_cursor == 'eraser'){
		canvas.style.cursor = 'cell';
	}
}
//save
function saveAsLocalImage(){
	var download = document.getElementById("download");
	var image = document.getElementById("MyCanvas").toDataURL("image/png")
    	.replace("image/png", "image/octet-stream");
	download.setAttribute("href", image);
}
//text
function textbutton(){
	on_shape = false;
	onshape = false;
	ontext = true;
	text_pos = true;
	cur_cursor = 'text';
	text_out = "";
	change_cursor();
	redraw();
}
function textfunction(event){
	if(ontext){
		if(event.key == 'Enter'){
			clickText.push(text_out);
			text_out = "";
			ontext = false;
			text_pos = false;
			if(eraser_on){
				cur_cursor = 'eraser';
			}else{
				cur_cursor = 'pencil';
			}
			change_cursor();
		}else if(event.key == 'Backspace'){
			text_out = text_out.slice(0, -1);
		}else{
			text_out += event.key;
		}
		redraw();
	}
}
function fontfunction(font){
	text_font = font;
	redraw();
}
function sizefunction(size){
	text_size = size;
	redraw();
}
//shape
function shapefunction(shape){
	shape_cur = shape;
	onshape = true;
}

//draw
function draw(){
	canvas.onmousedown = function(e){
		if(text_pos){
			textClick(e.pageX - this.offsetLeft, e.pageY - this.offsetTop);
			text_pos = false;
		}else if(onshape){
			on_shape = true;
			shapeClick(e.pageX - this.offsetLeft, e.pageY - this.offsetTop);
			shape_type.push(shape_cur);
			onshape = false;
		}else{
			if(ontext){
				ontext = false;
				text_out = "";
				textX.pop();
				textY.pop();
			}
			if(eraser_on){
				cur_cursor = 'eraser';
			}else{
				cur_cursor = 'pencil';
			}
			change_cursor();
			paint = true;
			addClick(e.pageX - this.offsetLeft, e.pageY - this.offsetTop);
			redraw();
		}
	}
	canvas.onmousemove = function(e){
		if(on_shape){
			if(shape_cur == 'circle'){
				var x = e.pageX - this.offsetLeft-shapeX[shapeX.length-1];
				var y = e.pageY - this.offsetTop-shapeY[shapeY.length-1];
				cir_rad_cur = Math.sqrt(x*x + y*y);
			}else if(shape_cur == 'rectangle'){
				rect_x = e.pageX - this.offsetLeft-shapeX[shapeX.length-1];
				rect_y = e.pageY - this.offsetTop-shapeY[shapeY.length-1]; 
			}else if(shape_cur == 'triangle'){
				tri_x = e.pageX - this.offsetLeft;
				tri_y = e.pageY - this.offsetTop;
			}
		}else if(paint){
		  addClick(e.pageX - this.offsetLeft, e.pageY - this.offsetTop, true);
		}
		redraw();
	}
	canvas.onmouseup = function(e){
		paint = false;
		if(on_shape){
			on_shape = false;
			if(shape_cur == 'circle'){
				cir_rad.push(cir_rad_cur);
				shape_rectx.push(0);
				shape_recty.push(0);
				shape_trix.push(0);
				shape_triy.push(0);
			}else if(shape_cur == 'rectangle'){
				cir_rad.push(0);
				shape_rectx.push(rect_x);
				shape_recty.push(rect_y);
				shape_trix.push(0);
				shape_triy.push(0);
			}else if(shape_cur == 'triangle'){
				cir_rad.push(0);
				shape_rectx.push(0);
				shape_recty.push(0);
				shape_trix.push(tri_x);
				shape_triy.push(tri_y);
			}
			redraw();
		}
	}
	canvas.onmouseleave = function(e){
		paint = false;
	}
	function textClick(x, y){
		textX.push(x);
		textY.push(y);
	}
	function shapeClick(x, y){
		console.log('add!');
		shapeX.push(x);
		shapeY.push(y);
		shape_color.push(eraser_col);
	}
	function addClick(x, y, dragging){
		clickX.push(x);
		clickY.push(y);
		clickDrag.push(dragging);
		clickColor.push(curcolor);
		clickSize.push(curSize);
	}
}
//redraw
function redraw(){
	clearcanvas_fn();
	context.lineJoin = "round";
	context.lineWidth = 5;
	context.font = text_size + " " + text_font;

	for(var i=0; i < clickX.length; i++) {		
	  context.beginPath();
	  if(clickDrag[i] && i){
		context.moveTo(clickX[i-1], clickY[i-1]);
	   }else{
		 context.moveTo(clickX[i]-1, clickY[i]);
	   }
	   context.lineTo(clickX[i], clickY[i]);
	   context.closePath();
	   context.strokeStyle = clickColor[i];
	   context.lineWidth = clickSize[i];
	   context.stroke();
	}
	var j = textX.length-1;
	if(ontext && !text_pos){
		context.fillText(text_out, textX[j], textY[j]);
	}
	for(var i=0; i < clickText.length; i++) {
		context.fillText(clickText[i], textX[i], textY[i]);
	}
	context.lineWidth = 4;
	if(on_shape){
		context.strokeStyle = shape_color[shape_color.length-1];
		if(shape_cur == 'circle'){
			context.beginPath();
			context.arc(shapeX[shapeX.length-1], shapeY[shapeY.length-1], cir_rad_cur, 0, 2*Math.PI, true);
			context.stroke();
		}else if(shape_cur == 'rectangle'){
			context.strokeRect(shapeX[shapeX.length-1], shapeY[shapeY.length-1], rect_x, rect_y);
		}else if(shape_cur == 'triangle'){
			var m = -(tri_x-shapeX[shapeX.length-1])/(tri_y-shapeY[shapeY.length-1]);
			var x = (tri_y-shapeY[shapeY.length-1])/Math.sqrt(3);
			context.beginPath();
			context.moveTo(shapeX[shapeX.length-1], shapeY[shapeY.length-1]);
			context.lineTo(tri_x+x, tri_y+x*m);
			context.lineTo(tri_x-x, tri_y-x*m);
			context.lineTo(shapeX[shapeX.length-1], shapeY[shapeY.length-1]);
			context.stroke();
		}
	}
	for(var i=0; i < shape_type.length; i++){
		context.strokeStyle = shape_color[i];
		if(shape_type[i] == 'circle'){
			context.beginPath();
			context.arc(shapeX[i], shapeY[i], cir_rad[i], 0, 2*Math.PI, true);
			context.stroke();
		}else if(shape_type[i] == 'rectangle'){
			context.strokeRect(shapeX[i], shapeY[i], shape_rectx[i], shape_recty[i]);
		}else if(shape_type[i] == 'triangle'){
			var m = -(shape_trix[i]-shapeX[i])/(shape_triy[i]-shapeY[i]);
			var x = (shape_triy[i]-shapeY[i])/Math.sqrt(3);
			context.beginPath();
			context.moveTo(shapeX[i], shapeY[i]);
			context.lineTo(shape_trix[i]+x, shape_triy[i]+x*m);
			context.lineTo(shape_trix[i]-x, shape_triy[i]-x*m);
			context.lineTo(shapeX[i], shapeY[i]);
			context.stroke();
		}
	}
}
function clearcanvas(){
	clickX = new Array();
	clickY = new Array();
	clickDrag = new Array();
	clickColor = new Array();
	clickSize = new Array();
	clickText = new Array();
	textX = new Array();
	textY = new Array();
	shapeX = new Array();
	shapeY = new Array();
	shape_type = new Array();
	cir_rad = new Array();
	shape_color = new Array();
	shape_rectx = new Array();
	shape_recty = new Array();
	shape_trix = new Array();
	shape_triy = new Array();
	clearcanvas_fn();
}
function clearcanvas_fn(){
	context.clearRect(0, 0, 1000, 580);
}